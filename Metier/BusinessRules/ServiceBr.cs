﻿using AutoMapper;
using Metier.AutoMapperProfile;

namespace Metier.BusinessRules
{
    public class ServiceBr
    {

        private readonly IMapper _mapper;
        public ServiceBr()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => { cfg.AddProfile(typeof(AutoMapperData)); });
            _mapper = config.CreateMapper();
        }
    }
}
