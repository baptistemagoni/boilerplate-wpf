﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Windows;
using WpfBoilerplate.Services;
using WpfBoilerplate.ViewModel;

namespace WpfBoilerplate
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MainViewModel _mainViewModel;

        public MainWindow()
        {
            InitializeComponent();
            _mainViewModel = IocKernel.Get<MainViewModel>();
            this.DataContext = _mainViewModel;
        }
        private void UcMenuMES_WPF_Loaded(object sender, RoutedEventArgs e)
        {
            UcMenuMES_WPF.UcMenuMES.OnFinInitialisation += UcMenuMES_OnFinInitialisation;
            //UcMenuMES_WPF.UcMenuMES.Initialise("MGV1", 1, "PDFCreator");
        }

        private void UcMenuMES_OnFinInitialisation(object sender, EventArgs e)
        {

        }
    }
}
