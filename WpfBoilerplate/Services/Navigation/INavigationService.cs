﻿using System;
using System.Collections.Generic;

namespace WpfBoilerplate.Services.Navigation
{
    public interface INavigationService
    {
        Enum CurrentPageKey { get; }
        void GoBack();
        void NavigateTo(Enum pageKey);
        void NavigateTo(Enum pageKey, object parameter);
        Dictionary<Enum, NavigationData> GetNavigations();
    }
}
