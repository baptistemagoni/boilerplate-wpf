﻿using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfBoilerplate.Services.Navigation
{
    public class NavigationService : INavigationService, INotifyPropertyChanged
    {
        #region Fields

        private readonly Dictionary<Enum, NavigationData> _navigationsData;
        private readonly List<Enum> _historic;
        private Enum _currentPageKey;

        #endregion

        #region Properties

        public Enum CurrentPageKey
        {
            get
            {
                return _currentPageKey;
            }

            private set
            {
                if (_currentPageKey == value)
                {
                    return;
                }
                _currentPageKey = value;
                OnPropertyChanged("CurrentPageKey");
            }
        }
        public object Parameter { get; private set; }

        #endregion

        #region Ctors and Methods

        public NavigationService(Dictionary<Enum, NavigationData> navigations)
        {
            _navigationsData = navigations;
            _historic = new List<Enum>();
        }
        public void GoBack()
        {
            if (_historic.Count > 1)
            {
                _historic.RemoveAt(_historic.Count - 1);
                NavigateTo(_historic.Last());
            }
        }
        public void NavigateTo(Enum pageKey)
        {
            NavigateTo(pageKey, null);
        }

        public virtual void NavigateTo(Enum pageKey, object parameter)
        {
            lock (_navigationsData)
            {
                if (!_navigationsData.ContainsKey(pageKey))
                {
                    throw new ArgumentException(string.Format("No such page: {0} ", pageKey), "pageKey");
                }

                var frame = GetDescendantFromName(Application.Current.MainWindow, "MainFrame") as Frame;

                if (frame != null)
                {
                    frame.Source = _navigationsData[pageKey].Uri;
                }
                Parameter = parameter;
                _historic.Add(pageKey);
                CurrentPageKey = pageKey;
            }
        }

        public void Configure(Enum key, NavigationData navigationData)
        {
            lock (_navigationsData)
            {
                if (_navigationsData.ContainsKey(key))
                {
                    _navigationsData[key] = navigationData;
                }
                else
                {
                    _navigationsData.Add(key, navigationData);
                }
            }
        }

        public Dictionary<Enum, NavigationData> GetNavigations()
            => _navigationsData;

        private static FrameworkElement GetDescendantFromName(DependencyObject parent, string name)
        {
            var count = VisualTreeHelper.GetChildrenCount(parent);

            if (count < 1)
            {
                return null;
            }

            for (var i = 0; i < count; i++)
            {
                var frameworkElement = VisualTreeHelper.GetChild(parent, i) as FrameworkElement;
                if (frameworkElement != null)
                {
                    if (frameworkElement.Name == name)
                    {
                        return frameworkElement;
                    }

                    frameworkElement = GetDescendantFromName(frameworkElement, name);
                    if (frameworkElement != null)
                    {
                        return frameworkElement;
                    }
                }
            }
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public class NavigationData
    {
        public string Path { get; set; }

        public Uri Uri { get; set; }
    }
}
