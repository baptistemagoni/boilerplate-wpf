﻿namespace WpfBoilerplate.Services.Store
{
    public interface IStore
    {

        void SetValue<TClass>(StoreEnum action, TClass @class) where TClass : class;

        TClass GetValue<TClass>(StoreEnum action) where TClass : class;

        bool RemoveValue(StoreEnum action);

    }
}
