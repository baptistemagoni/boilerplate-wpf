﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfBoilerplate.Services.Store
{
    public class Store : IStore
    {

        private Dictionary<StoreEnum, object> _stores;

        public Store()
        {
            _stores = new Dictionary<StoreEnum, object>();
        }

        public TClass GetValue<TClass>(StoreEnum action) where TClass : class
        {
            object result = _stores.FirstOrDefault(x => x.Key == action).Value;
            return result is TClass ? result as TClass : throw new Exception();
        }

        public bool RemoveValue(StoreEnum action)
        {
            return _stores.Remove(action);
        }

        public void SetValue<TClass>(StoreEnum action, TClass @class) where TClass : class
        {
            var result = _stores.FirstOrDefault(x => x.Key == action).Value;
            if(result == null) _stores.Add(action, @class);
            else result = @class;
        }
    }
}
