using GalaSoft.MvvmLight.Command;
using Socle.Composants.ChangeUser;
using System;
using System.Windows;
using WpfBoilerplate.Class;
using WpfBoilerplate.Services.Navigation;

namespace WpfBoilerplate.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        #region Fields

        private INavigationService _navigationService;

        #endregion

        #region DataBinding

        private Visibility _pageMaintenanceVisibility;

        public Visibility PageMaintenanceVisibility
        {
            get => _pageMaintenanceVisibility;
            set => SetProperty(ref _pageMaintenanceVisibility, value);
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            PageMaintenanceVisibility = Visibility.Collapsed;
        }

        #endregion

        #region Commands

        private RelayCommand _displayHomeCommand;

        public RelayCommand DisplayHomeCommand
        {
            get
            {
                if (_displayHomeCommand == null)
                    _displayHomeCommand = new RelayCommand(() => DisplayHome());
                return _displayHomeCommand;
            }
        }

        private void DisplayHome()
        {
            _navigationService.NavigateTo(ApplicationTypeEnum.HOME);
        }

        private RelayCommand _displayAccountConnectionCommand;

        public RelayCommand DisplayAccountConnectionCommand
        {
            get
            {
                if (_displayAccountConnectionCommand == null)
                    _displayAccountConnectionCommand = new RelayCommand(() => DisplayAccountConnection());
                return _displayAccountConnectionCommand;
            }
        }

        private void DisplayAccountConnection()
        {
            FormUserChange formUserChange = new FormUserChange();
            formUserChange.User.OnUserChange += PageListOFViewModel_OnUserChange;
            formUserChange.ShowDialog();
        }

        private void PageListOFViewModel_OnUserChange(object sender, EventArgs e, UserType userType)
        {
            PageMaintenanceVisibility = userType == UserType.Maintenance || userType == UserType.Administrateur ? Visibility.Visible : Visibility.Hidden;
        }

        private RelayCommand _displaySettingsCommand;

        public RelayCommand DisplaySettingsCommand
        {
            get
            {
                if (_displaySettingsCommand == null)
                    _displaySettingsCommand = new RelayCommand(() => DisplaySettings());
                return _displaySettingsCommand;
            }
        }

        private void DisplaySettings()
        {
            _navigationService.NavigateTo(ApplicationTypeEnum.SETTINGS);
        }

        #endregion
    }
}