﻿using AutoMapper;
using GalaSoft.MvvmLight.Command;
using Socle.Composants.Configuration;
using Socle.Resources.Langues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using WpfBoilerplate.ViewModel;

namespace ApplicationCuvesConnectees.ViewModel
{
    public class SettingsViewModel : ViewModelBase
    {

        #region Fields

        private EventHandlerChangeSelection OnChangeSelection;
        private delegate void EventHandlerChangeSelection(object sender);
        private List<IConfigurationCommonClient> _configurationCommons;
        private List<SelectedData> _valuesUpdate;
        private IMapper _mapper;
        private ConfigurationBackup _sauvegardeConfig;

        #endregion

        #region Events

        public void ChangeSelectionData(object sender)
        {
            _valuesUpdate.AddRange(_selectedData.Where(x => x.ValueUpdate).ToList());
            SelectedData.Clear();
            List<SelectedData> temps = new List<SelectedData>();
            foreach (PropertyInfo item in _selectedConfiguration.Configuration.GetType().GetProperties())
            {
                List<object> comboBoxList = new List<object>();
                if (item.PropertyType == typeof(Boolean)) comboBoxList.AddRange(new object[] { true, false });
                else if (item.PropertyType.BaseType == typeof(Enum)) comboBoxList.AddRange(Enum.GetValues(item.PropertyType).Cast<Enum>());
                else if (item.PropertyType == typeof(System.Collections.IEnumerable)) comboBoxList.AddRange((IEnumerable<object>)item.GetValue(_selectedConfiguration.Configuration));
                temps.Add(new SelectedData { Name = item.Name, Value = Convert.ToString(item.GetValue(_selectedConfiguration.Configuration, null)), ConfigurationCommonClient = _selectedConfiguration.Configuration, ValueUpdate = false, Type = item.PropertyType, ConboxBoxItems = comboBoxList });
            }
            SelectedData = temps;
        }

        #endregion

        #region DataBinding

        private List<ConfigurationData> _configuration;

        /// <summary>
        /// Cette variable permet de stocker tous les types de configuration de l'application (WebService, ...)
        /// </summary>
        public List<ConfigurationData> Configurations
        {
            get => _configuration;
            set => SetProperty(ref _configuration, value);
        }

        private List<SelectedData> _selectedData;

        /// <summary>
        /// Cette variable permet de stocker toutes les configurations de l'élément selectionné
        /// </summary>
        public List<SelectedData> SelectedData
        {
            get => _selectedData;
            set => SetProperty(ref _selectedData, value);
        }

        private ConfigurationData _selectedConfiguration;

        /// <summary>
        /// Cette vraible permet d'avoir la configuration actuel selectionnée
        /// </summary>
        public ConfigurationData SelectedConfiguration
        {
            get => _selectedConfiguration;
            set
            {
                if (value != _selectedConfiguration)
                {
                    SetProperty(ref _selectedConfiguration, value);
                    try { OnChangeSelection?.Invoke(this); }
                    catch (Exception e) { }
                }
            }
        }

        #endregion

        #region Commands

        public RelayCommand SaveConfigurationCommand { get; private set; }

        public void Save()
        {
            try
            {
                _valuesUpdate.ForEach(valueUpdate =>
                {
                    PropertyInfo info = valueUpdate.ConfigurationCommonClient.GetType().GetProperty(valueUpdate.Name);
                    info.SetValue(valueUpdate.ConfigurationCommonClient, Convert.ChangeType(valueUpdate.Value, info.PropertyType));
                    valueUpdate.ConfigurationCommonClient.SaveConfiguration();
                });
                _selectedData.ForEach(selectedData =>
                {
                    PropertyInfo info = _selectedConfiguration.Configuration.GetType().GetProperty(selectedData.Name);
                    Type dataType = info.PropertyType;
                    info.SetValue(_selectedConfiguration.Configuration, dataType.BaseType == typeof(Enum) ? Enum.Parse(dataType, selectedData.Value.ToString()) : Convert.ChangeType(selectedData.Value, dataType));
                });
                _selectedConfiguration.Configuration.SaveConfiguration();

                if (MessageBox.Show(Langue.Info_RedemarrageSouhaitez, "Enregistrer tout", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                    Application.Current.Shutdown();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public RelayCommand BackupConfigurationCommand { get; private set; }

        public void Backup()
        {
            try
            {
                if (_sauvegardeConfig == null)
                {
                    _sauvegardeConfig = new ConfigurationBackup();
                    _sauvegardeConfig.Security = "Armor\\CH_TFS_CONFIG@tf$c0nFig";
                }
                _sauvegardeConfig.StartBackup();
            }
            catch (Exception ex)
            {

            }
        }

        public RelayCommand RestoreConfigurationCommand { get; private set; }

        public void Restore()
        {
            try
            {
                try
                {
                    if (MessageBox.Show(Langue.Info_RestaurerConfiguration, "Restauration", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
                    {
                        if (_sauvegardeConfig == null)
                        {
                            _sauvegardeConfig = new ConfigurationBackup();
                            _sauvegardeConfig.Security = "Armor\\CH_TFS_CONFIG@tf$c0nFig";
                        }
                        _sauvegardeConfig.Restore();
                        MessageBox.Show(Langue.Info_RestaurationConfigurationEffective, "Restauration", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Constructors

        public SettingsViewModel(IMapper mapper)
        {
            _mapper = mapper;
            OnChangeSelection += ChangeSelectionData;
            SelectedData = new List<SelectedData>();
            _valuesUpdate = new List<SelectedData>();
            _configurationCommons = new List<IConfigurationCommonClient>();
            //Initialize and call configuration here
            Configurations = _mapper.Map<List<ConfigurationData>>(_configurationCommons);
            SaveConfigurationCommand = new RelayCommand(() => Save());
            BackupConfigurationCommand = new RelayCommand(() => Backup());
            RestoreConfigurationCommand = new RelayCommand(() => Restore());
        }

        #endregion

        #region Internals methods

        #endregion

    }

    public class ConfigurationData
    {

        public string Name { get; set; }

        public IConfigurationCommonClient Configuration { get; set; }

        public Byte[] Image { get; set; }

    }

    public class SelectedData : ViewModelBase
    {

        public bool ValueUpdate { get; set; }

        public IConfigurationCommonClient ConfigurationCommonClient { get; set; }

        private Type _type;

        public Type Type
        {
            get => _type;
            set => SetProperty(ref _type, value);
        }

        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private object _value;

        public object Value
        {
            get => _value;
            set
            {
                ValueUpdate = value != _value;
                SetProperty(ref _value, value);
            }
        }

        private string _description;

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        private List<object> _conboxBoxItems;

        public List<object> ConboxBoxItems
        {
            get => _conboxBoxItems;
            set => SetProperty(ref _conboxBoxItems, value);
        }
    }
}
