
using CommonServiceLocator;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using WpfBoilerplate.Class;
using WpfBoilerplate.Services;
using WpfBoilerplate.Services.Navigation;
using WpfBoilerplate.Services.Store;

namespace WpfBoilerplate.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        public MainViewModel MainViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            IocKernel.Initialize(new AppCuvesConnecteesIocModule(CreateNavigationService()));
        }

        private Dictionary<Enum, NavigationData> CreateNavigationService()
        {
            Dictionary<Enum, NavigationData> navigations = new Dictionary<Enum, NavigationData>();
            navigations.Add(ApplicationTypeEnum.HOME, new NavigationData { Path = "Home", Uri = new Uri("../Views/PageHome.xaml", UriKind.Relative) });
            navigations.Add(ApplicationTypeEnum.SETTINGS, new NavigationData { Path = "Settings", Uri = new Uri("../Views/PageSettings.xaml", UriKind.Relative) });
            return navigations;
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }

    internal class AppCuvesConnecteesIocModule : NinjectModule
    {

        private Dictionary<Enum, NavigationData> _navigationData;

        public AppCuvesConnecteesIocModule(Dictionary<Enum, NavigationData> navigations)
        {
            this._navigationData = navigations;
        }

        public override void Load()
        {
            Bind<INavigationService>()
                .To<NavigationService>()
                .InSingletonScope()
                .WithConstructorArgument("navigations", _navigationData);

            Bind<IStore>()
                .To<Store>()
                .InSingletonScope();
        }
    }
}