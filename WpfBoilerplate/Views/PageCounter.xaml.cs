﻿using ModuleComposantWpf.Themes;
using ModuleComposantWpf.ViewModel;
using System.Windows;
using System.Windows.Controls;
using WpfBoilerplate.ViewModel;

namespace WpfBoilerplate.View
{
    /// <summary>
    /// Logique d'interaction pour PageCounter.xaml
    /// </summary>
    public partial class PageCounter : Page
    {

        private CounterViewModel _model { get; set; }

        public PageCounter()
        {
            InitializeComponent();
            _model = new CounterViewModel();
            this.DataContext = _model; // Init view model for create binding with the counter label
        }

        private void CounterButtonUp_Click(object sender, RoutedEventArgs e)
        {
            _model.Counter += 1;
        }

        private void CounterButtonDown_Click(object sender, RoutedEventArgs e)
        {
            _model.Counter -= 1;
        }

        private void CallModal_Click(object sender, RoutedEventArgs e)
        {

            ModalViewModel model = new ModalViewModel { Title = "Ceci est un test" };
            ModalComponent modal = new ModalComponent { Model = model, Element = new PageError(), ModalWidth = 500.0, ModalHeight = 300.0 }.Build();
            modal.ShowDialog();
        }
    }
}
