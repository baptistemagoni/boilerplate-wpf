﻿using ApplicationCuvesConnectees.ViewModel;
using System.Windows.Controls;
using WpfBoilerplate.Services;

namespace WpfBoilerplate.Views
{
    /// <summary>
    /// Logique d'interaction pour PageSettings.xaml
    /// </summary>
    public partial class PageSettings : Page
    {

        private SettingsViewModel _model;
        public PageSettings()
        {
            InitializeComponent();
            _model = IocKernel.Get<SettingsViewModel>();
            this.DataContext = _model;
        }
    }
}
